class personas {
    constructor ( unNombre, unApellido, unaEdad){
        this.nombre = unNombre;
        this.apellido = unApellido;
        this.edad = unaEdad;
    }

    obtenerNombreCompleto(){
        return `${this.nombre} ${this.apellido}`;

    }

    esMayor(){
        return this.edad >= 18;
    }
