var numero1 = 5;
var numero2 = 7;
var numero3 = 1;
var numero4 = 15;
var numero5 = 15;
var numero6 = 8;
var numero7 = 2;

if (numero1 > numero2){
    console.log('La variable numero1 ('+ numero1 +') es mayor a numero2 (' + numero2 + ')')
} else {
    console.log('La variable numero1 ('+ numero1 +') NO es mayor a numero2 (' + numero2 + ')')
}

if (numero2 > numero3 && numero2 > numero7) {
    console.log('La variable numero2 ('+ numero2 +') es mayor a numero3 ('+ numero3 +') Y mayor a numero7 ('+ numero7 +')');
} else{
    console.log('La variable numero2 ('+ numero2 +') NO es mayor a numero3 ('+ numero3 +') O no es mayor a numero7 ('+numero7+')')
}

if (numero4 == numero7) {
    console.log('La variable numero4 ('+ numero4 +') es igual a numero7 ('+ numero7 +')')
} else {
    console.log('La variable numero4 ('+ numero4 +') NO es igual a numero7 ('+numero7+')')
}

switch (numero7) {
    case 1:
        console.log("El número es analizado es 1");
        break;
    case 2:
        console.log("El número es analizado es 2");
        break;    
    case 3:
        console.log("El número es analizado es 3");
        break;        

    default:
        console.log("El número NO es ninguno de los anteriores");
        break;
}

//resultado = (condiciones) ? verdadero : falso;
var resultado = (numero4 > numero5) ? true : false;
console.log(resultado);